-- Generation time: Wed, 30 Nov 2022 13:43:11 -0300
-- Host: localhost
-- DB name: oneven08_tlv_challender
/*!40030 SET NAMES UTF8 */;
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

DROP TABLE IF EXISTS `audit`;
CREATE TABLE `audit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `page` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `ip` varchar(255) NOT NULL,
  `viewed` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

INSERT INTO `audit` VALUES ('1','0','91','2022-11-30 13:23:10','::1','0'),
('2','0','3','2022-11-30 13:23:14','::1','0'),
('3','0','91','2022-11-30 13:36:42','::1','0'); 


DROP TABLE IF EXISTS `crons`;
CREATE TABLE `crons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` int(1) NOT NULL DEFAULT 1,
  `sort` int(3) NOT NULL,
  `name` varchar(255) NOT NULL,
  `file` varchar(255) NOT NULL,
  `createdby` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

INSERT INTO `crons` VALUES ('1','0','100','Auto-Backup','backup.php','1','2017-09-16 07:49:22','2017-11-11 17:15:36'); 


DROP TABLE IF EXISTS `crons_logs`;
CREATE TABLE `crons_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cron_id` int(11) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



DROP TABLE IF EXISTS `email`;
CREATE TABLE `email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `website_name` varchar(100) NOT NULL,
  `smtp_server` varchar(100) NOT NULL,
  `smtp_port` int(10) NOT NULL,
  `email_login` varchar(150) NOT NULL,
  `email_pass` varchar(100) NOT NULL,
  `from_name` varchar(100) NOT NULL,
  `from_email` varchar(150) NOT NULL,
  `transport` varchar(255) NOT NULL,
  `verify_url` varchar(255) NOT NULL,
  `email_act` int(1) NOT NULL,
  `debug_level` int(1) NOT NULL DEFAULT 0,
  `isSMTP` int(1) NOT NULL DEFAULT 0,
  `isHTML` varchar(5) NOT NULL DEFAULT 'true',
  `useSMTPauth` varchar(6) NOT NULL DEFAULT 'true',
  `authtype` varchar(50) DEFAULT 'CRAM-MD5',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

INSERT INTO `email` VALUES ('1','User Spice','smtp.gmail.com','587','yourEmail@gmail.com','1234','User Spice','yourEmail@gmail.com','tls','http://localhost/userspice','0','0','0','true','true','CRAM-MD5'); 


DROP TABLE IF EXISTS `groups_menus`;
CREATE TABLE `groups_menus` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(11) unsigned NOT NULL,
  `menu_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`),
  KEY `menu_id` (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4;

INSERT INTO `groups_menus` VALUES ('5','0','3'),
('6','0','1'),
('7','0','2'),
('8','0','51'),
('9','0','52'),
('10','0','37'),
('11','0','38'),
('12','2','39'),
('13','2','40'),
('14','2','41'),
('15','2','42'),
('16','2','43'),
('17','2','44'),
('18','2','45'),
('19','0','46'),
('20','0','47'),
('21','0','49'),
('25','0','18'),
('26','0','20'),
('27','0','21'),
('28','0','7'),
('29','0','8'),
('30','2','9'),
('31','2','10'),
('32','2','11'),
('33','2','12'),
('34','2','13'),
('35','2','14'),
('36','2','15'),
('37','0','16'),
('38','1','15'); 


DROP TABLE IF EXISTS `keys`;
CREATE TABLE `keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stripe_ts` varchar(255) NOT NULL,
  `stripe_tp` varchar(255) NOT NULL,
  `stripe_ls` varchar(255) NOT NULL,
  `stripe_lp` varchar(255) NOT NULL,
  `recap_pub` varchar(100) NOT NULL,
  `recap_pri` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



DROP TABLE IF EXISTS `logs`;
CREATE TABLE `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `logdate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `logtype` varchar(25) NOT NULL,
  `lognote` mediumtext NOT NULL,
  `ip` varchar(75) DEFAULT NULL,
  `metadata` blob DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4;

INSERT INTO `logs` VALUES ('1','1','2022-11-29 18:53:02','System Updates','Update 2022-05-04a successfully deployed.','::1',NULL),
('2','0','2022-11-29 18:53:10','Login Fail','A failed login on login.php','::1',NULL),
('3','1','2022-11-29 18:53:15','login','User logged in.','::1',NULL),
('4','1','2022-11-29 22:55:24','Form Data','admin_settings.php View Here--->','::1','{\"post\":{\"value\":\"true\",\"field\":\"bleeding_edge\",\"desc\":\"Bleeding Edge\",\"type\":\"toggle\"}}'),
('5','1','2022-11-29 22:55:26','Form Data','admin.php View Here--->','::1','{\"get\":{\"view\":\"general\"}}'),
('6','1','2022-11-29 22:55:35','Form Data','admin.php View Here--->','::1','{\"get\":{\"view\":\"backup\"}}'),
('7','1','2022-11-29 22:55:47','Form Data','admin.php View Here--->','::1','{\"get\":{\"view\":\"backup\"},\"post\":{\"backup_dest\":\"\\/\",\"backup_source\":\"db_only\",\"backup_table\":\"audit\",\"backup\":\"Backup\"}}'),
('8','1','2022-11-29 23:03:39','Form Data','admin.php View Here--->','::1','{\"get\":{\"view\":\"backup\"},\"post\":{\"backup_dest\":\"\\/\",\"backup_source\":\"db_only\",\"backup_table\":\"audit\",\"backup\":\"Backup\"}}'),
('9','1','2022-11-29 23:03:47','Form Data','admin.php View Here--->','::1','{\"get\":{\"view\":\"backup\"}}'),
('10','1','2022-11-29 23:03:53','Form Data','admin.php View Here--->','::1','{\"get\":{\"view\":\"backup\"},\"post\":{\"backup_dest\":\"\\/\",\"backup_source\":\"db_only\",\"backup_table\":\"audit\",\"backup\":\"Backup\"}}'),
('11','1','2022-11-29 23:04:45','Form Data','admin.php View Here--->','::1','{\"get\":{\"view\":\"backup\"}}'),
('12','1','2022-11-29 23:04:47','Form Data','admin.php View Here--->','::1','{\"get\":{\"view\":\"backup\"},\"post\":{\"backup_dest\":\"\\/\",\"backup_source\":\"db_only\",\"backup_table\":\"audit\",\"backup\":\"Backup\"}}'),
('13','1','2022-11-29 19:04:48','Admin Backup','Completed backup for Database.','::1',NULL),
('14','1','2022-11-30 10:48:42','Redirect Diag','From logout.php on line 4 to /tlv_challenger/index.php','::1',NULL),
('15','0','2022-11-30 11:00:09','Login Fail','A failed login on login.php','::1',NULL),
('16','1','2022-11-30 11:00:14','login','User logged in.','::1',NULL),
('17','1','2022-11-30 15:00:29','Form Data','admin.php View Here--->','::1','{\"get\":{\"view\":\"users\"}}'),
('18','1','2022-11-30 15:05:36','Form Data','admin.php View Here--->','::1','{\"get\":{\"view\":\"users\"},\"post\":{\"username\":\"tripulante\",\"fname\":\"Ronald\",\"lname\":\"McNair\",\"email\":\"tripulante@tlv.ag\",\"csrf\":\"37b895f65315e7b28f66f393b1622655\",\"addUser\":\"Add User\"}}'),
('19','1','2022-11-30 11:05:37','User Manager','Added user tripulante.','::1',NULL),
('20','1','2022-11-30 11:05:37','Redirect Diag','From admin.php on line 146 to /tlv_challenger/users/admin.php?view=user&amp;id=3','::1',NULL),
('21','1','2022-11-30 15:05:37','Form Data','admin.php View Here--->','::1','{\"get\":{\"view\":\"user\",\"id\":\"3\"}}'),
('22','1','2022-11-30 15:05:48','Form Data','admin.php View Here--->','::1','{\"get\":{\"view\":\"user\",\"id\":\"3\"},\"post\":{\"unx\":\"tripulante\",\"emx\":\"tripulante@tlv.ag\",\"fnx\":\"Ronald\",\"lnx\":\"McNair\",\"pwx\":\"#TLV@2023@@\",\"cloak_allowed\":\"0\",\"active\":\"1\",\"force_pr\":\"0\",\"dev_user\":\"0\",\"protected\":\"0\",\"csrf\":\"37b895f65315e7b28f66f393b1622655\",\"return\":\"Update &amp; Close\"}}'),
('23','1','2022-11-30 11:05:48','Redirect Diag','From admin.php on line 195 to admin.php?view=user&amp;id=3','::1',NULL),
('24','1','2022-11-30 15:05:48','Form Data','admin.php View Here--->','::1','{\"get\":{\"view\":\"user\",\"id\":\"3\"}}'),
('25','1','2022-11-30 15:06:03','Form Data','admin.php View Here--->','::1','{\"get\":{\"view\":\"user\",\"id\":\"3\"},\"post\":{\"unx\":\"tripulante\",\"emx\":\"tripulante@tlv.ag\",\"fnx\":\"Ronald\",\"lnx\":\"McNair\",\"pwx\":\"\",\"cloak_allowed\":\"0\",\"active\":\"1\",\"force_pr\":\"0\",\"dev_user\":\"0\",\"protected\":\"0\",\"csrf\":\"37b895f65315e7b28f66f393b1622655\",\"return\":\"Update &amp; Close\"}}'),
('26','1','2022-11-30 11:06:04','Redirect Diag','From admin.php on line 370 to admin.php?view=users','::1',NULL),
('27','1','2022-11-30 15:06:04','Form Data','admin.php View Here--->','::1','{\"get\":{\"view\":\"users\"}}'),
('28','1','2022-11-30 15:08:18','Form Data','admin.php View Here--->','::1','{\"get\":{\"view\":\"general\"}}'),
('29','1','2022-11-30 15:08:28','Form Data','admin_settings.php View Here--->','::1','{\"post\":{\"value\":\"pt-BR\",\"field\":\"default_language\",\"desc\":\"Default Language\",\"type\":\"txt\"}}'),
('30','1','2022-11-30 15:08:50','Form Data','admin.php View Here--->','::1','{\"get\":{\"view\":\"general\"}}'),
('31','1','2022-11-30 15:08:57','Form Data','admin_settings.php View Here--->','::1','{\"post\":{\"value\":\"en-US\",\"field\":\"default_language\",\"desc\":\"Default Language\",\"type\":\"txt\"}}'),
('32','1','2022-11-30 15:08:59','Form Data','admin.php View Here--->','::1','{\"get\":{\"view\":\"general\"}}'),
('33','1','2022-11-30 15:09:04','Form Data','admin_settings.php View Here--->','::1','{\"post\":{\"value\":\"true\",\"field\":\"allow_language\",\"desc\":\"Allow user to change language setting\",\"type\":\"toggle\"}}'),
('34','1','2022-11-30 15:09:10','Form Data','index.php View Here--->','::1','{\"post\":{\"your_token\":\"::1\",\"language_selector\":\"1\",\"pt-BR_x\":\"30\",\"pt-BR_y\":\"12\",\"csrf\":\"\"}}'),
('35','1','2022-11-30 15:09:37','Form Data','admin.php View Here--->','::1','{\"get\":{\"view\":\"general\"}}'),
('36','1','2022-11-30 15:09:47','Form Data','admin_settings.php View Here--->','::1','{\"post\":{\"value\":\"TLV Challenger\",\"field\":\"site_name\",\"desc\":\"Site Name\",\"type\":\"txt\"}}'),
('37','1','2022-11-30 15:09:53','Form Data','admin_settings.php View Here--->','::1','{\"post\":{\"value\":\"tlv.ag\",\"field\":\"copyright\",\"desc\":\"Copyright Message\",\"type\":\"txt\"}}'),
('38','1','2022-11-30 11:15:35','Redirect Diag','From logout.php on line 4 to /tlv_challenger/index.php','::1',NULL),
('39','1','2022-11-30 11:16:15','login','User logged in.','::1',NULL),
('40','1','2022-11-30 15:16:24','Form Data','admin.php View Here--->','::1','{\"get\":{\"view\":\"reg\"}}'),
('41','1','2022-11-30 15:16:27','Form Data','admin_settings.php View Here--->','::1','{\"post\":{\"value\":\"false\",\"field\":\"registration\",\"desc\":\"Registration System\",\"type\":\"toggle\"}}'),
('42','1','2022-11-30 11:16:36','Redirect Diag','From logout.php on line 4 to /tlv_challenger/index.php','::1',NULL),
('43','3','2022-11-30 11:45:49','login','User logged in.','::1',NULL),
('44','1','2022-11-30 11:51:19','login','User logged in.','::1',NULL),
('45','1','2022-11-30 15:51:24','Form Data','admin.php View Here--->','::1','{\"get\":{\"view\":\"pages\"}}'),
('46','1','2022-11-30 15:51:30','Form Data','admin.php View Here--->','::1','{\"get\":{\"view\":\"page\",\"id\":\"92\"}}'),
('47','1','2022-11-30 15:51:35','Form Data','admin.php View Here--->','::1','{\"get\":{\"view\":\"page\",\"id\":\"92\"},\"post\":{\"process\":\"1\",\"private\":\"Yes\",\"addPermission\":[\"1\",\"2\"],\"changeTitle\":\"\",\"csrf\":\"b6501c9af88403008e0f2d1dac8c374b\",\"return\":\"Update &amp; Close\"}}'),
('48','1','2022-11-30 11:51:35','Pages Manager','Added 2 permission(s) to relatorio.php.','::1',NULL),
('49','1','2022-11-30 11:51:35','Redirect Diag','From admin.php on line 113 to admin.php?view=pages','::1',NULL),
('50','1','2022-11-30 15:51:35','Form Data','admin.php View Here--->','::1','{\"get\":{\"view\":\"pages\"}}'),
('51','1','2022-11-30 15:51:37','Form Data','admin.php View Here--->','::1','{\"get\":{\"view\":\"page\",\"id\":\"91\"}}'),
('52','1','2022-11-30 15:51:40','Form Data','admin.php View Here--->','::1','{\"get\":{\"view\":\"page\",\"id\":\"91\"},\"post\":{\"process\":\"1\",\"private\":\"Yes\",\"addPermission\":[\"1\",\"2\"],\"changeTitle\":\"\",\"csrf\":\"b6501c9af88403008e0f2d1dac8c374b\",\"return\":\"Update &amp; Close\"}}'),
('53','1','2022-11-30 11:51:40','Pages Manager','Added 2 permission(s) to crud.php.','::1',NULL),
('54','1','2022-11-30 11:51:40','Redirect Diag','From admin.php on line 113 to admin.php?view=pages','::1',NULL),
('55','1','2022-11-30 15:51:40','Form Data','admin.php View Here--->','::1','{\"get\":{\"view\":\"pages\"}}'),
('56','1','2022-11-30 15:51:42','Form Data','admin.php View Here--->','::1','{\"get\":{\"view\":\"page\",\"id\":\"90\"}}'),
('57','1','2022-11-30 15:51:45','Form Data','admin.php View Here--->','::1','{\"get\":{\"view\":\"page\",\"id\":\"90\"},\"post\":{\"process\":\"1\",\"private\":\"Yes\",\"addPermission\":[\"1\",\"2\"],\"changeTitle\":\"\",\"csrf\":\"b6501c9af88403008e0f2d1dac8c374b\",\"return\":\"Update &amp; Close\"}}'),
('58','1','2022-11-30 11:51:45','Pages Manager','Added 2 permission(s) to ajax.php.','::1',NULL),
('59','1','2022-11-30 11:51:45','Redirect Diag','From admin.php on line 113 to admin.php?view=pages','::1',NULL),
('60','1','2022-11-30 15:51:45','Form Data','admin.php View Here--->','::1','{\"get\":{\"view\":\"pages\"}}'),
('61','1','2022-11-30 13:19:16','Redirect Diag','From logout.php on line 4 to /tlv_challenger/index.php','::1',NULL),
('62','3','2022-11-30 13:20:16','login','User logged in.','::1',NULL),
('63','1','2022-11-30 13:42:45','login','User logged in.','::1',NULL),
('64','1','2022-11-30 17:43:07','Form Data','admin.php View Here--->','::1','{\"get\":{\"view\":\"backup\"}}'),
('65','1','2022-11-30 17:43:11','Form Data','admin.php View Here--->','::1','{\"get\":{\"view\":\"backup\"},\"post\":{\"backup_dest\":\"\\/\",\"backup_source\":\"db_only\",\"backup_table\":\"audit\",\"backup\":\"Backup\"}}'); 


DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `menu_title` varchar(255) NOT NULL,
  `parent` int(10) NOT NULL,
  `dropdown` int(1) NOT NULL,
  `logged_in` int(1) NOT NULL,
  `display_order` int(10) NOT NULL,
  `label` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `icon_class` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4;

INSERT INTO `menus` VALUES ('1','main','2','0','1','1','{{home}}','','fa fa-fw fa-home'),
('2','main','-1','1','1','14','','','fa fa-fw fa-cogs'),
('3','main','-1','0','1','11','{{username}}','users/account.php','fa fa-fw fa-user'),
('4','main','-1','1','0','3','{{help}}','','fa fa-fw fa-life-ring'),
('5','main','-1','0','0','2','{{register}}','users/join.php','fa fa-fw fa-plus-square'),
('6','main','-1','0','0','1','{{login}}','users/login.php','fa fa-fw fa-sign-in'),
('7','main','2','0','1','2','{{account}}','users/account.php','fa fa-fw fa-user'),
('8','main','2','0','1','3','{{hr}}','',''),
('9','main','2','0','1','4','{{dashboard}}','users/admin.php','fa fa-fw fa-cogs'),
('10','main','2','0','1','5','{{users}}','users/admin.php?view=users','fa fa-fw fa-user'),
('11','main','2','0','1','6','{{perms}}','users/admin.php?view=permissions','fa fa-fw fa-lock'),
('12','main','2','0','1','7','{{pages}}','users/admin.php?view=pages','fa fa-fw fa-wrench'),
('13','main','2','0','1','9','{{logs}}','users/admin.php?view=logs','fa fa-fw fa-search'),
('14','main','2','0','1','10','{{hr}}','',''),
('15','main','2','0','1','11','{{logout}}','users/logout.php','fa fa-fw fa-sign-out'),
('16','main','-1','0','0','0','{{home}}','','fa fa-fw fa-home'),
('17','main','-1','0','1','10','{{home}}','','fa fa-fw fa-home'),
('18','main','4','0','0','1','{{forgot}}','users/forgot_password.php','fa fa-fw fa-wrench'),
('20','main','4','0','0','99999','{{resend}}','users/verify_resend.php','fa fa-exclamation-triangle'); 


DROP TABLE IF EXISTS `message_threads`;
CREATE TABLE `message_threads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_to` int(11) NOT NULL,
  `msg_from` int(11) NOT NULL,
  `msg_subject` varchar(255) NOT NULL,
  `last_update` datetime NOT NULL,
  `last_update_by` int(11) NOT NULL,
  `archive_from` int(1) NOT NULL DEFAULT 0,
  `archive_to` int(1) NOT NULL DEFAULT 0,
  `hidden_from` int(1) NOT NULL DEFAULT 0,
  `hidden_to` int(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_from` int(11) NOT NULL,
  `msg_to` int(11) NOT NULL,
  `msg_body` mediumtext NOT NULL,
  `msg_read` int(1) NOT NULL,
  `msg_thread` int(11) NOT NULL,
  `deleted` int(1) NOT NULL,
  `sent_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



DROP TABLE IF EXISTS `notifications`;
CREATE TABLE `notifications` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `message` longtext NOT NULL,
  `is_read` tinyint(4) NOT NULL,
  `is_archived` tinyint(1) DEFAULT 0,
  `date_created` datetime DEFAULT NULL,
  `date_read` datetime DEFAULT NULL,
  `last_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `class` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `private` int(11) NOT NULL DEFAULT 0,
  `re_auth` int(1) NOT NULL DEFAULT 0,
  `core` int(1) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8mb4;

INSERT INTO `pages` VALUES ('1','index.php','Home','0','0','1'),
('2','z_us_root.php','','0','0','1'),
('3','users/account.php','Account Dashboard','1','0','1'),
('4','users/admin.php','Admin Dashboard','1','0','1'),
('14','users/forgot_password.php','Forgotten Password','0','0','1'),
('15','users/forgot_password_reset.php','Reset Forgotten Password','0','0','1'),
('16','users/index.php','Home','0','0','1'),
('17','users/init.php','','0','0','1'),
('18','users/join.php','Join','0','0','1'),
('19','users/joinThankYou.php','Join','0','0','1'),
('20','users/login.php','Login','0','0','1'),
('21','users/logout.php','Logout','0','0','1'),
('24','users/user_settings.php','User Settings','1','0','1'),
('25','users/verify.php','Account Verification','0','0','1'),
('26','users/verify_resend.php','Account Verification','0','0','1'),
('45','users/maintenance.php','Maintenance','0','0','1'),
('68','users/update.php','Update Manager','1','0','1'),
('81','users/admin_pin.php','Verification PIN Set','1','0','1'),
('90','ajax.php',NULL,'1','0','0'),
('91','crud.php',NULL,'1','0','0'),
('92','relatorio.php',NULL,'1','0','0'); 


DROP TABLE IF EXISTS `permission_page_matches`;
CREATE TABLE `permission_page_matches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_id` int(11) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8mb4;

INSERT INTO `permission_page_matches` VALUES ('3','1','24'),
('14','2','4'),
('15','1','3'),
('38','2','68'),
('54','1','81'),
('58','1','92'),
('59','2','92'),
('60','1','91'),
('61','2','91'),
('62','1','90'),
('63','2','90'); 


DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

INSERT INTO `permissions` VALUES ('1','User'),
('2','Administrator'); 


DROP TABLE IF EXISTS `profiles`;
CREATE TABLE `profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `bio` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

INSERT INTO `profiles` VALUES ('1','1','&lt;h1&gt;This is the Admin&#039;s bio.&lt;/h1&gt;'),
('2','2','This is your bio'); 


DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `recaptcha` int(1) NOT NULL DEFAULT 0,
  `force_ssl` int(1) NOT NULL,
  `css_sample` int(1) NOT NULL,
  `site_name` varchar(100) NOT NULL,
  `language` varchar(255) NOT NULL,
  `site_offline` int(1) NOT NULL,
  `force_pr` int(1) NOT NULL,
  `glogin` int(1) NOT NULL DEFAULT 0,
  `fblogin` int(1) NOT NULL,
  `gid` varchar(255) NOT NULL,
  `gsecret` varchar(255) NOT NULL,
  `gredirect` varchar(255) NOT NULL,
  `ghome` varchar(255) NOT NULL,
  `fbid` varchar(255) NOT NULL,
  `fbsecret` varchar(255) NOT NULL,
  `fbcallback` varchar(255) NOT NULL,
  `graph_ver` varchar(255) NOT NULL,
  `finalredir` varchar(255) NOT NULL,
  `req_cap` int(1) NOT NULL,
  `req_num` int(1) NOT NULL,
  `min_pw` int(2) NOT NULL,
  `max_pw` int(3) NOT NULL,
  `min_un` int(2) NOT NULL,
  `max_un` int(3) NOT NULL,
  `messaging` int(1) NOT NULL,
  `snooping` int(1) NOT NULL,
  `echouser` int(11) NOT NULL,
  `wys` int(1) NOT NULL,
  `change_un` int(1) NOT NULL,
  `backup_dest` varchar(255) NOT NULL,
  `backup_source` varchar(255) NOT NULL,
  `backup_table` varchar(255) NOT NULL,
  `msg_notification` int(1) NOT NULL,
  `permission_restriction` int(1) NOT NULL,
  `auto_assign_un` int(1) NOT NULL,
  `page_permission_restriction` int(1) NOT NULL,
  `msg_blocked_users` int(1) NOT NULL,
  `msg_default_to` int(1) NOT NULL,
  `notifications` int(1) NOT NULL,
  `notif_daylimit` int(3) NOT NULL,
  `recap_public` varchar(100) NOT NULL,
  `recap_private` varchar(100) NOT NULL,
  `page_default_private` int(1) NOT NULL,
  `navigation_type` tinyint(1) NOT NULL,
  `copyright` varchar(255) NOT NULL,
  `custom_settings` int(1) NOT NULL,
  `system_announcement` varchar(255) NOT NULL,
  `twofa` int(1) DEFAULT 0,
  `force_notif` tinyint(1) DEFAULT NULL,
  `cron_ip` varchar(255) DEFAULT NULL,
  `registration` tinyint(1) DEFAULT NULL,
  `join_vericode_expiry` int(9) unsigned NOT NULL,
  `reset_vericode_expiry` int(9) unsigned NOT NULL,
  `admin_verify` tinyint(1) NOT NULL,
  `admin_verify_timeout` int(9) NOT NULL,
  `session_manager` tinyint(1) NOT NULL,
  `template` varchar(255) DEFAULT 'standard',
  `saas` tinyint(1) DEFAULT NULL,
  `redirect_uri_after_login` mediumtext DEFAULT NULL,
  `show_tos` tinyint(1) DEFAULT 1,
  `default_language` varchar(11) DEFAULT NULL,
  `allow_language` tinyint(1) DEFAULT NULL,
  `spice_api` varchar(75) DEFAULT NULL,
  `announce` datetime DEFAULT NULL,
  `bleeding_edge` tinyint(1) DEFAULT 0,
  `err_time` int(11) DEFAULT 15,
  `container_open_class` varchar(255) DEFAULT 'container-fluid',
  `debug` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

INSERT INTO `settings` VALUES ('1','0','0','0','TLV Challenger','en','0','0','0','0','','','','','','','','','','0','0','6','150','4','30','0','1','0','1','0','/','db_only','','0','0','0','0','0','1','0','7','6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI','6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe','1','1','tlv.ag','1','','0','0','off','0','24','15','1','120','0','standard',NULL,NULL,'1','en-US','1',NULL,'2022-11-30 11:00:14','1','15','container-fluid','1'); 


DROP TABLE IF EXISTS `updates`;
CREATE TABLE `updates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `migration` varchar(15) NOT NULL,
  `applied_on` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `update_skipped` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8mb4;

INSERT INTO `updates` VALUES ('15','1XdrInkjV86F','2018-02-18 19:33:24',NULL),
('16','3GJYaKcqUtw7','2018-04-25 13:51:08',NULL),
('17','3GJYaKcqUtz8','2018-04-25 13:51:08',NULL),
('18','69qa8h6E1bzG','2018-04-25 13:51:08',NULL),
('19','2XQjsKYJAfn1','2018-04-25 13:51:08',NULL),
('20','549DLFeHMNw7','2018-04-25 13:51:08',NULL),
('21','4Dgt2XVjgz2x','2018-04-25 13:51:08',NULL),
('22','VLBp32gTWvEo','2018-04-25 13:51:08',NULL),
('23','Q3KlhjdtxE5X','2018-04-25 13:51:08',NULL),
('24','ug5D3pVrNvfS','2018-04-25 13:51:08',NULL),
('25','69FbVbv4Jtrz','2018-04-25 13:51:09',NULL),
('26','4A6BdJHyvP4a','2018-04-25 13:51:09',NULL),
('27','37wvsb5BzymK','2018-04-25 13:51:09',NULL),
('28','c7tZQf926zKq','2018-04-25 13:51:09',NULL),
('29','ockrg4eU33GP','2018-04-25 13:51:09',NULL),
('30','XX4zArPs4tor','2018-04-25 13:51:09',NULL),
('31','pv7r2EHbVvhD','2018-04-25 21:00:00',NULL),
('32','uNT7NpgcBDFD','2018-04-25 21:00:00',NULL),
('33','mS5VtQCZjyJs','2018-12-11 11:19:16',NULL),
('34','23rqAv5elJ3G','2018-12-11 11:19:51',NULL),
('35','qPEARSh49fob','2019-01-01 09:01:01',NULL),
('36','FyMYJ2oeGCTX','2019-01-01 09:01:01',NULL),
('37','iit5tHSLatiS','2019-01-01 09:01:01',NULL),
('38','hcA5B3PLhq6E','2020-07-16 08:27:53',NULL),
('39','VNEno3E4zaNz','2020-07-16 08:27:53',NULL),
('40','2ZB9mg1l0JXe','2020-07-16 08:27:53',NULL),
('41','B9t6He7qmFXa','2020-07-16 08:27:53',NULL),
('42','86FkFVV4TGRg','2020-07-16 08:27:53',NULL),
('43','y4A1Y0u9n2Rt','2020-07-16 08:27:53',NULL),
('44','Tm5xY22MM8eC','2020-07-16 08:27:53',NULL),
('45','0YXdrInkjV86F','2020-07-16 08:27:53',NULL),
('46','99plgnkjV86','2020-07-16 08:27:53',NULL),
('47','0DaShInkjV86','2020-07-16 08:27:53',NULL),
('48','0DaShInkjVz1','2020-07-16 08:27:53',NULL),
('49','y4A1Y0u9n2SS','2020-07-16 08:27:53',NULL),
('50','0DaShInkjV87','2020-07-16 08:27:53',NULL),
('51','0DaShInkjV88','2020-07-16 08:27:53',NULL),
('52','2019-09-04a','2020-07-16 08:27:53',NULL),
('53','2019-09-05a','2020-07-16 08:27:53',NULL),
('54','2019-09-26a','2020-07-16 08:27:53',NULL),
('55','2019-11-19a','2020-07-16 08:27:53',NULL),
('56','2019-12-28a','2020-07-16 08:27:53',NULL),
('57','2020-01-21a','2020-07-16 08:27:54',NULL),
('58','2020-03-26a','2020-07-16 08:27:54',NULL),
('59','2020-04-17a','2020-07-16 08:27:54',NULL),
('60','2020-06-06a','2020-07-16 08:27:54',NULL),
('61','2020-06-30a','2020-07-16 08:27:54',NULL),
('62','2020-07-01a','2020-07-16 08:27:54',NULL),
('63','2020-07-16a','2020-10-07 22:26:22',NULL),
('64','2020-07-30a','2020-10-07 22:26:22',NULL),
('65','2020-10-06a','2022-04-15 14:37:11',NULL),
('66','2020-11-03a','2022-04-15 14:37:11',NULL),
('67','2020-11-08a','2022-04-15 14:37:11',NULL),
('68','2020-11-10a','2022-04-15 14:37:11',NULL),
('69','2020-11-10b','2022-04-15 14:37:11',NULL),
('70','2020-12-17a','2022-04-15 14:37:11',NULL),
('71','2020-12-28a','2022-04-15 14:37:11',NULL),
('72','2021-01-20a','2022-04-15 14:37:11',NULL),
('73','2021-02-16a','2022-04-15 14:37:11',NULL),
('74','2021-04-14a','2022-04-15 14:37:11',NULL),
('75','2021-04-15a','2022-04-15 14:37:11',NULL),
('76','2021-05-20a','2022-04-15 14:37:11',NULL),
('77','2021-07-11a','2022-04-15 14:37:11',NULL),
('78','2021-08-22a','2022-04-15 14:37:11',NULL),
('79','2021-08-24a','2022-04-15 14:37:11',NULL),
('80','2021-09-25a','2022-04-15 14:37:11',NULL),
('81','2021-12-26a','2022-04-15 14:37:11',NULL),
('82','2022-05-04a','2022-11-29 18:53:02',NULL); 


DROP TABLE IF EXISTS `us_announcements`;
CREATE TABLE `us_announcements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dismissed` int(11) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `ignore` varchar(50) DEFAULT NULL,
  `class` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



DROP TABLE IF EXISTS `us_fingerprint_assets`;
CREATE TABLE `us_fingerprint_assets` (
  `kFingerprintAssetID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fkFingerprintID` int(11) NOT NULL,
  `IP_Address` varchar(255) NOT NULL,
  `User_Browser` varchar(255) NOT NULL,
  `User_OS` varchar(255) NOT NULL,
  PRIMARY KEY (`kFingerprintAssetID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



DROP TABLE IF EXISTS `us_fingerprints`;
CREATE TABLE `us_fingerprints` (
  `kFingerprintID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fkUserID` int(11) NOT NULL,
  `Fingerprint` varchar(32) NOT NULL,
  `Fingerprint_Expiry` datetime NOT NULL,
  `Fingerprint_Added` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`kFingerprintID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



DROP TABLE IF EXISTS `us_form_validation`;
CREATE TABLE `us_form_validation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `params` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

INSERT INTO `us_form_validation` VALUES ('1','min','Minimum # of Characters','number'),
('2','max','Maximum # of Characters','number'),
('3','is_numeric','Must be a number','true'),
('4','valid_email','Must be a valid email address','true'),
('5','<','Must be a number less than','number'),
('6','>','Must be a number greater than','number'),
('7','<=','Must be a number less than or equal to','number'),
('8','>=','Must be a number greater than or equal to','number'),
('9','!=','Must not be equal to','text'),
('10','==','Must be equal to','text'),
('11','is_integer','Must be an integer','true'),
('12','is_timezone','Must be a valid timezone name','true'),
('13','is_datetime','Must be a valid DateTime','true'); 


DROP TABLE IF EXISTS `us_form_views`;
CREATE TABLE `us_form_views` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_name` varchar(255) NOT NULL,
  `view_name` varchar(255) NOT NULL,
  `fields` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



DROP TABLE IF EXISTS `us_forms`;
CREATE TABLE `us_forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



DROP TABLE IF EXISTS `us_ip_blacklist`;
CREATE TABLE `us_ip_blacklist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(50) NOT NULL,
  `last_user` int(11) NOT NULL DEFAULT 0,
  `reason` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;



DROP TABLE IF EXISTS `us_ip_list`;
CREATE TABLE `us_ip_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

INSERT INTO `us_ip_list` VALUES ('2','::1','1','2022-11-30 13:42:45'); 


DROP TABLE IF EXISTS `us_ip_whitelist`;
CREATE TABLE `us_ip_whitelist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;



DROP TABLE IF EXISTS `us_management`;
CREATE TABLE `us_management` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page` varchar(255) NOT NULL,
  `view` varchar(255) NOT NULL,
  `feature` varchar(255) NOT NULL,
  `access` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;

INSERT INTO `us_management` VALUES ('1','_admin_manage_ip.php','ip','IP Whitelist/Blacklist',''),
('2','_admin_nav.php','nav','Navigation [List/Add/Delete]',''),
('3','_admin_nav_item.php','nav_item','Navigation [View/Edit]',''),
('4','_admin_pages.php','pages','Page Management [List]',''),
('5','_admin_page.php','page','Page Management [View/Edit]',''),
('6','_admin_security_logs.php','security_logs','Security Logs',''),
('7','_admin_templates.php','templates','Templates',''),
('8','_admin_tools_check_updates.php','updates','Check Updates',''); 


DROP TABLE IF EXISTS `us_plugin_hooks`;
CREATE TABLE `us_plugin_hooks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page` varchar(255) NOT NULL,
  `folder` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,
  `hook` varchar(255) NOT NULL,
  `disabled` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



DROP TABLE IF EXISTS `us_plugins`;
CREATE TABLE `us_plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plugin` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `updates` mediumtext DEFAULT NULL,
  `last_check` datetime DEFAULT '2020-01-01 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



DROP TABLE IF EXISTS `us_saas_levels`;
CREATE TABLE `us_saas_levels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` varchar(255) NOT NULL,
  `users` int(11) NOT NULL,
  `details` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



DROP TABLE IF EXISTS `us_saas_orgs`;
CREATE TABLE `us_saas_orgs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `org` varchar(255) NOT NULL,
  `owner` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `active` int(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



DROP TABLE IF EXISTS `us_user_sessions`;
CREATE TABLE `us_user_sessions` (
  `kUserSessionID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fkUserID` int(11) unsigned NOT NULL,
  `UserFingerprint` varchar(255) NOT NULL,
  `UserSessionIP` varchar(255) NOT NULL,
  `UserSessionOS` varchar(255) NOT NULL,
  `UserSessionBrowser` varchar(255) NOT NULL,
  `UserSessionStarted` datetime NOT NULL,
  `UserSessionLastUsed` datetime DEFAULT NULL,
  `UserSessionLastPage` varchar(255) NOT NULL,
  `UserSessionEnded` tinyint(1) NOT NULL DEFAULT 0,
  `UserSessionEnded_Time` datetime DEFAULT NULL,
  PRIMARY KEY (`kUserSessionID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;



DROP TABLE IF EXISTS `user_permission_matches`;
CREATE TABLE `user_permission_matches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=utf8mb4;

INSERT INTO `user_permission_matches` VALUES ('100','1','1'),
('101','1','2'),
('111','3','1'); 


DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissions` tinyint(1) NOT NULL,
  `email` varchar(155) NOT NULL,
  `email_new` varchar(155) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `pin` varchar(255) DEFAULT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `language` varchar(255) DEFAULT 'en-US',
  `email_verified` tinyint(1) NOT NULL DEFAULT 0,
  `vericode` varchar(15) DEFAULT NULL,
  `vericode_expiry` datetime DEFAULT NULL,
  `oauth_provider` varchar(255) DEFAULT NULL,
  `oauth_uid` varchar(255) DEFAULT NULL,
  `gender` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `gpluslink` varchar(255) DEFAULT NULL,
  `account_owner` tinyint(4) NOT NULL DEFAULT 1,
  `account_id` int(11) NOT NULL DEFAULT 0,
  `account_mgr` int(11) NOT NULL DEFAULT 0,
  `fb_uid` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL,
  `protected` tinyint(1) NOT NULL DEFAULT 0,
  `msg_exempt` tinyint(1) NOT NULL DEFAULT 0,
  `dev_user` tinyint(1) NOT NULL DEFAULT 0,
  `msg_notification` tinyint(1) NOT NULL DEFAULT 1,
  `cloak_allowed` tinyint(1) NOT NULL DEFAULT 0,
  `oauth_tos_accepted` tinyint(1) DEFAULT NULL,
  `un_changed` tinyint(1) NOT NULL DEFAULT 0,
  `force_pr` tinyint(1) NOT NULL DEFAULT 0,
  `logins` int(11) unsigned NOT NULL DEFAULT 0,
  `last_login` datetime DEFAULT NULL,
  `join_date` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `EMAIL` (`email`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

INSERT INTO `users` VALUES ('1','1','userspicephp@userspice.com',NULL,'admin','$2y$12$1v06jm2KMOXuuo3qP7erTuTIJFOnzhpds1Moa8BadnUUeX0RV3ex.',NULL,'The','Admin','pt-BR','1','nlPsJDtyeqFWsS',NULL,'','','','','','1','0','0','','','0000-00-00 00:00:00','1','1','0','1','1',NULL,'0','0','5','2022-11-30 13:42:45','2016-01-01 00:00:00','2016-01-01 00:00:00','1'),
('3','1','tripulante@tlv.ag',NULL,'tripulante','$2y$12$mDwozQPcnzlJFv1TZsMM8uSn1C7BSonFMAZj1EZpp6GFn4IoGopfe',NULL,'Ronald','McNair','en-US','1','eIRjlnbjErrxOv','2022-11-30 11:21:04',NULL,NULL,'','',NULL,'1','0','0',NULL,NULL,'0000-00-00 00:00:00','0','0','0','1','0','1','0','0','2','2022-11-30 13:20:16','2022-11-30 11:05:36',NULL,'1'); 


DROP TABLE IF EXISTS `users_online`;
CREATE TABLE `users_online` (
  `id` int(11) NOT NULL,
  `ip` varchar(15) NOT NULL,
  `timestamp` varchar(15) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `session` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



DROP TABLE IF EXISTS `users_session`;
CREATE TABLE `users_session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `uagent` mediumtext DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;





/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

