<?php
if (file_exists("install/index.php")) {
    //perform redirect if installer files exist
    //this if{} block may be deleted once installed
    header("Location: install/index.php");
}

require_once 'users/init.php';
require_once $abs_us_root . $us_url_root . 'users/includes/template/prep.php';
if (isset($user) && $user->isLoggedIn()) {
}
?>


<div class="jumbotron">
	<h1 align="center"><?= lang("JOIN_SUC"); ?>
		<?php echo $settings->site_name; ?>
	</h1>
	<p align="center" class="text-muted">
		Uma estrutura de gerencimento de usuarios, paginas e mais algumas coisas para ajudar na produtividade.<br>
		O modelo é simples, com seu próprio framework e pode ser utilizado com absolutamente qualquer coisa.

	</p>

	<h1> Lista de desafios</h1>
	<ul class="list-group">
		<li class="list-group-item "><a href="crud.php">1 - Fazer um CRUD basico para cadastro dos
				Ganhadores de um ingresso, contendo Nome, Email, Telefone e CPF e Categoria(Vip, Corporate, Premium)
			</a>
			<br><small>A forma que você irá fazer fica a sua escolha ( se são entre
				paginas, modais e ajax). Será avaliado a performance do codigo, cuidados com segurança nos inputs,
				componentes e modelagem de dados, além das soluções mais criativas</small>
		</li>
		<li class="list-group-item ">
			<a href="users/account.php">2 - Ao Clicar na pagina Minha Conta
				(users/accounts.php) e depois na users/user_settings.php, não conseguimos alterar nossa imagem </a>
			<br><small>Só aparece a imagem se você utlizar uma email cadastrado no Gravatar.com. O programador nao
				entendeu isso e precisamos corrigir! Devemos ter a possibilidade de fazer um upload e mostrar nossa
				foto. Deve ter até um campo no banco pra isso.<br>
				Só tome cuidado coma segurança, lembre-se disso.
			</small>
		</li>
		<li class="list-group-item ">
			<a href="relatorio.php">3 - Criar um Dashboard para o cadastro de ganhadores </a>
			<br><small>Nesse dashboard devemos ter um topo com os seguintes indicadores : Total por Vip, Total Premium e
				Total Corporate.
				Logo abaixo uma listagem dessa tabela, obadecendo o clique desses indicadores no topo : Clicou em VIP,
				deve-se mostrar a listagem
				apenas dos VIP.
				<br>
				A aginação é impotante mas não urgente, ok? Ouvimos dizer que existe um tal de datatable.net que ajuda,
				mas enfim... Faça do jeito mais rapido e bonito! O cliente pediu de ultima hora.
			</small>
		</li>
		<li class="list-group-item ">
			<a href="api.php">4 - Consuma uma API REST </a>
			<br><small>Aqui precisamos que você consuma uma API REST pubica, qualquer uma que achar. Pode ser por JS,
				pode ser via cUrl, tanto faz
				<br>
				Use a criatividade e apresente algo legal pra gente!
			</small>
		</li>

	</ul>

	<br>
	<h3>Dicas e instruções</h3>
	<p>
		<i class="fa fa-check-circle-o" aria-hidden="true"></i>
		Após a realização de cada desafio você pode usar a classe list-group-item-success para marcar a linha como
		feita,
		e ajuda-lo na organização, exemplo:
		<li class="list-group-item "> Item Não Finalizado </li>
		<li class="list-group-item list-group-item-success"> Item Finalizado </li>


	</p>
	<br>
	<p>
		<i class="fa fa-check-circle-o" aria-hidden="true"></i>
		Todas essas paginas já estão criadas, bastanto clicar no link do desafio. Só implementar, mas pra isso vocÊ
		precisa
		estar logado. Use as credenciais : <br>
		<br> - login : tripulante
		<br> - senha : #TLV@2023@@
	</p>



	<p>
		<i class="fa fa-check-circle-o" aria-hidden="true"></i>
		Todas as paginas ja estão conectadas ao equema de banco de dados.
		<br>
		<br>Para fazer um select utilize instrução :
		<br>
		<b>
			$users = $db->query("SELECT username FROM users");
		</b>
		<br>Ele vai trazer um array $users e você pode iterar com o foreach ou for
		<br>
		<hr>

		Para fazer um update, pode utilizar o metodo query ou utilize instrução :
		<br>
		<b>
			$result = $db->update("tabela",ID_do_registro, ['Nome do campo' => 'Valor']);
		</b>
		<br>Ele vai responder com a quantidade de itens afetados no banco
		<br>
		<hr>
		Para fazer um insert, pode utilizar o metodo query ou utilize instrução :
		<br>
		<b>
			$result = $db->insert("tabela", ['Nome do campo' => 'Valor']);
		</b>
		<br>Ele vai responder com a quantidade de itens afetados no banco
		<br>
		<hr>
		Para fazer um delete, pode utilizar o metodo query ou utilize instrução :
		<br>
		<b>
			$result = $db->delete("tabela", id_do_registro);
		</b>
		<br>Ele vai responder com a quantidade de itens afetados no banco
		<br>
		<hr>

		Dica : Acesse users/classes/DB.php para ter acesso a todos os métodos dessa abstração.





	</p>



	<p align="center">
		<?php
        if ($user->isLoggedIn()) { ?>
		<a class="btn btn-primary" href="users/account.php"
			role="button"><?= lang("ACCT_HOME"); ?>
			&raquo;</a>
		<?php } else { ?>
		<a class="btn btn-warning" href="users/login.php" role="button">
			<?php
            echo "ACESSAR MINHA CONTA E INICIAR OS DESAFIOS"
            //lang("SIGNIN_TEXT");
		    ?>
			&raquo;</a>

		<?php } ?>
	</p>
	<br>

</div>
<?php languageSwitcher(); ?>


<!-- Place any per-page javascript here -->
<?php require_once $abs_us_root . $us_url_root . 'users/includes/html_footer.php'; ?>